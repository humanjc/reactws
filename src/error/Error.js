import React from 'react';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';

class Error extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFinish: false
    }
    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit() {
    this.setState({
      isFinish: true
    });
  }

  render() {
    const {isFinish} = this.state;
    if(isFinish) {
      return <Redirect to='/' />;
    }
    let message = 'Unknown Error!';
    if(_.get(this.props, 'match.params.errorNumber', '') === '404') {
      message = 'Page Not Found (404)';
    }
    return (
      <div>
        <div className='panel center'>
          <div>
            <span>Error</span>
          </div>
          <div>
            <div>{message}</div>
          </div>
          <div>
            <button 
              type='submit'
              onClick={() => this.handlerSubmit()}
            >
              OK
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Error;
