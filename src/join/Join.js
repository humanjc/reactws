import React from 'react';
import { Redirect } from 'react-router-dom';

class Join extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFinish: false
    }
    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit() {
    this.setState({
      isFinish: true
    });
  }

  render() {
    const {isFinish} = this.state;
    if(isFinish) {
      return <Redirect to='/' />;
    }
    return (
      <div>
        <div className='panel center'>
          <div>
            <span>Join</span>
          </div>
          <div>
            <label>id :</label>
            <input 
              type='text' 
              id='id' 
            />
          </div>
          <div>
            <label>name :</label>
            <input type='text' id='name' />
          </div>
          <div>
            <label>passwd :</label>
            <input type='text' id='passwd' />
          </div>
          <div>
            <button 
              type='submit'
              onClick={() => this.handlerSubmit()}
            >
              Join
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Join;
