import * as types from './ActionTypes';

export function setUserId(userId) {
  return {
      type: types.SET_USER_ID,
      userId
  };
}

