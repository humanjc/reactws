import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';
import {connect} from 'react-redux';
import { Segment, Header, Label, Input, Button } from 'semantic-ui-react'
import * as actions from '../actions';

class Login extends React.Component {
  static propTypes = {
    userId: PropTypes.string,
    handlerSetUserId: PropTypes.func
  };

  static defaultProps = {
    userId: '',
    handlerSetUserId: () => console.warn('function is not defined!')
  };

  constructor(props) {
    super(props);
    const userId = sessionStorage.getItem('reactws_user_id');
    this.state = {
      userId: userId !== null ? userId : this.props.userId,
      inputUserId: ''
    };
    if(userId !== null) {
      this.props.handlerSetUserId(userId);
    }
    this.handlerIdChange = this.handlerIdChange.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    let shouldUpdate = true;
    if(this.props.userId !== nextProps.userId) {
      shouldUpdate = false;
      this.setState({userId: nextProps.userId});
    }
    return shouldUpdate;
  }    

  handlerIdChange(inputUserId) {
    this.setState({
      inputUserId
    });
  }

  render() {
    const {inputUserId} = this.state;
    if(this.props.userId !== '') {
      return <Redirect to='/' />;
    }
    return (
      <div>
        <Segment className='panel center'>
          <Header>
            <span>Login</span>
          </Header>
          <div>
            <Label>id :</Label>
            <Input 
              id='id' 
              value={inputUserId} 
              onChange={event => this.handlerIdChange(event.target.value)}
            />
          </div>
          <div>
            <Label>passwd :</Label>
            <Input id='passwd' />
          </div>
          <div>
            <Link to="/join">회원 가입</Link>
            <Button
              onClick={() => this.props.handlerSetUserId(inputUserId)}
            >
              Login
            </Button>
          </div>
        </Segment>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userId: state.userId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handlerSetUserId: (userId) => {
      dispatch(actions.setUserId(userId));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
