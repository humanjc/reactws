import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../actions';

class Top extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    from: PropTypes.string,
    handlerSetUserId: PropTypes.func,
  }

  static defaultProps = {
    title: '',
    from: '/',
    handlerSetUserId: () => console.warn('under construct!'),
  }

  render() {
    const {title, from} = this.props;
    return (
      <div id='top'>
        <div className='left'>{title}</div>
        <div className='right'>
          <Link 
            to={{
              pathname: '/info',
              state: {
                url: {from}
              }
            }}
          >
            회원 정보
          </Link>
          <button 
            type='submit'
            onClick={() => this.props.handlerSetUserId('')}
          >
            Logout
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handlerSetUserId: (userId) => {
      dispatch(actions.setUserId(userId));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Top);
