import React from 'react';
import {Route, Redirect} from "react-router-dom";
import _ from 'lodash';
import Top from './Top';
import Left from './Left';
import Right from './Right';
import First from './First';
import Second from './Second';
import Third from './Third';

class Home extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      menus: [{
        id: 1,
        name: 'first'
      },{
        id: 2,
        name: 'second'
      },{
        id: 3,
        name: 'third'
      }],
      timeStamp: 0,
    };
    // setTimeout(this.addTimeStamp, 5000);
  }

  componentDidMount() {
    this._isMounted = true; 
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // addTimeStamp = () => {
  //   if (this._isMounted) {
  //     this.setState({timeStamp: this.state.timeStamp + 1});
  //     setTimeout(this.addTimeStamp, 5000);
  //   }
  // };

  render() {
    if(_.get(this.props, 'match.params.page', '') === '') {
      return <Redirect to='/pages/first' />;
    }
    const {menus, timeStamp} = this.state;
    const from = _.get(this.props, 'location.pathname', '/');
    return (
      <div className='home'>
        <Top 
          title='React Workshop'
          from={from}
          />
        <div className='content'>
          <Left 
            menus={menus}
          />
          <Right>
            <Route
              exact path={['/', '/pages/first']}
            >
              <First
                timeStamp={timeStamp}
              />
            </Route>
            <Route
              component={Second}
              path={['/pages/second']}
            />
            <Route
              component={Third}
              path={['/pages/third']}
            />
          </Right>
        </div>
       </div>
    );
  }
}

export default Home;
