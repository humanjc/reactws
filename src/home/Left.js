import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";
import _ from 'lodash';

class Left extends React.Component {
  static propTypes = {
    menus: PropTypes.array
  }

  static defaultProps = {
    menus: []
  }
  
  render() {
    const {menus} = this.props;
    const menu = _.map(menus, menu => (
      <li 
        key={menu.id}
      >
      <NavLink 
        to={'/pages/' + _.lowerCase(menu.name)} 
        activeClassName="active"
      >
        {menu.name}
      </NavLink>
    </li>
    ));
    return (
      <div id='left'>
        <ul>{menu}</ul>
      </div>
    );
  }
}

export default Left;
