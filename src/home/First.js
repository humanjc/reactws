import React from 'react';
import PropTypes from 'prop-types';

class First extends React.Component {
  static propTypes = {
    timeStamp: PropTypes.number
  }

  static defaultProps = {
    timeStamp: 0
  }
      
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
    this.handlerButtonClick = this.handlerButtonClick.bind(this);    
  }
  
  handlerButtonClick() {
    this.setState({
      count: this.state.count + 1
    });
  }  

  shouldComponentUpdate(nextProps, nextState) {
    let shouldUpdate = true;
    if(nextState.count > 2) {
      shouldUpdate = false;
    }
    return shouldUpdate;
  }  
  
  render() {
    return (
      <div className='content'>
        <h1>first</h1>
        <div>
          <span>count = {this.state.count}</span>
          <button 
            onClick={this.handlerButtonClick} >
            add 1
          </button>
        </div>
        <div>timeStamp = {this.props.timeStamp}</div>
      </div>
    );
  }
}

export default First;
