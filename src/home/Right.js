import React from 'react';
import PropTypes from 'prop-types';

class Right extends React.Component {
  static propTypes = {
    children: PropTypes.array
  }

  static defaultProps = {
    children: null
  }

  render() {
    return (
      <div id='right'>
        {this.props.children}
      </div>
    );
  }
}

export default Right;
