import {combineReducers} from 'redux';
import auth from './auth';

const reducers = combineReducers({
    userId: auth
});

export default reducers;
