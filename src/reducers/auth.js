import * as types from '../actions/ActionTypes';
import initialState from '../config/initialState';

export default function userId(state = initialState.userId, action) {
  if(action.type === types.SET_USER_ID) {
    sessionStorage.setItem('reactws_user_id', action.userId);
    return action.userId;
  } else {
    return state;
  }
}
