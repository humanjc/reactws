import React from 'react';
import PropTypes from 'prop-types';
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from './actions';
import Home from './home/Home';
import Login from './login/Login';
import Join from './join/Join';
import Info from './info/Info';
import Error from './error/Error';
import 'semantic-ui-less/semantic.less'
import './App.css';

class App extends React.Component {
  static propTypes = {
    userId: PropTypes.string,
    handlerSetUserId: PropTypes.func
  };

  static defaultProps = {
    userId: '',
    handlerSetUserId: () => console.warn('function is not defined!')
  };

  constructor(props) {
    super(props);
    const userId = sessionStorage.getItem('reactws_user_id');
    this.state = {
      userId: userId !== null ? userId : this.props.userId
    }
    if(userId !== null) {
      this.props.handlerSetUserId(userId);
    }
    this.AuthRoute = this.AuthRoute.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    let shouldUpdate = true;
    if(this.props.userId !== nextProps.userId) {
      shouldUpdate = false;
      this.setState({userId: nextProps.userId});
    }
    return shouldUpdate;
  }  

  AuthRoute({ children, ...rest }) {
    if(this.state.userId === '') {
      return (
        <Redirect
          to={{
            pathname: "/login"
          }}
        />
      )      
    }
    return (
      <React.Fragment>
        <Route
          component={Home}
          exact path={['/', '/pages/:page']}
        />
        <Route
          component={Info}
          path='/info'
        />
      </React.Fragment>
    );
  }

  render() {
    const AuthRoute = this.AuthRoute;
    return (
      <BrowserRouter>
        <Switch>
          <Route
            component={Login}
            path='/login'
          />
          <Route
            component={Join}
            path='/join'
          />
          <Route
            component={Error}
            path='/error/:errorNumber'
          />
          <AuthRoute 
            component={Home}
            exact path={['/', '/pages/:page', '/info']}
          />
          <Redirect from='*' to='/error/404' />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    userId: state.userId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handlerSetUserId: (userId) => {
      dispatch(actions.setUserId(userId));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);